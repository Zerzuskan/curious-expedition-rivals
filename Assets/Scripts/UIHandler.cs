﻿using UnityEngine;

namespace Assets.Scripts
{
    public class UIHandler : MonoBehaviour
    {
        private Ray ray;
        RaycastHit hit;

        void Start()
        {
            Cursor.visible = true;
            Cursor.lockState = CursorLockMode.None;
        }

        void Update()
        {
            ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            if (Physics.Raycast(ray, out hit))
            {
                Debug.Log(hit.transform.name);
            }
        }
    }
}