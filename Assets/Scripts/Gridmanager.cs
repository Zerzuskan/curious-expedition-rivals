﻿using UnityEngine;

namespace Assets.Scripts
{
    public class Gridmanager : MonoBehaviour
    {
        [SerializeField]
        private Camera Camera;

        [SerializeField]
        private int rows = 2;
        [SerializeField]
        private int cols = 3;

        public GameObject[,] LocationReferences;

        [SerializeField]
        private float tileSize = 1f;

        void Start()
        {
            LocationReferences = new GameObject[rows, cols];
            GenerateGrid();
        }

        private void GenerateGrid()
        {
            GameObject referenceTile = (GameObject)Instantiate(Resources.Load("Grasstile"));
            for (int i = 0; i < rows; i++)
            {
                for (int j = 0; j < cols; j++)
                {
                    GameObject tile = Instantiate(referenceTile, transform);
                    float posX = j * tileSize;
                    float posY = i * -tileSize;

                    tile.transform.position = new Vector2(posX,posY);
                    LocationReferences[i, j] = tile;
                }
            }

            Destroy(referenceTile);
            float gridW = cols * tileSize;
            float gridH = rows * tileSize;
            transform.position = new Vector3(-gridW / 2 + tileSize / 2, (float)rows/2, gridH / 2 - tileSize / 2);

            Camera.orthographicSize = ((float)rows/2)*1.05f;
        }
    }
}